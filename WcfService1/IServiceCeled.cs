﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using CeledDA;

namespace WcfService1
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract(Namespace = "http://www.celed.mx")]
    public interface IServiceCeled
    {
        /// <summary>
        /// TypeQuery 1 Clientes dados desde alta el dia de ayer
        /// TypeQuery 2 Todos los clientes que exiten
        /// </summary>
        /// <param name="TypeQuery"></param>
        /// <returns></returns>
        [OperationContract]
        string GenerateFileQuerys(int TypeQuery);
        /// <summary>
        /// GetDataUsingDataContract
        /// </summary>
        /// <param name="composite"></param>
        /// <returns></returns>
        CompositeType GetDataUsingDataContract(CompositeType composite);
        /// <summary>
        /// Crea una orden de venta enviada de www.saintech.com a Sap
        /// </summary>
        /// <param name="orders"></param>
        /// <returns></returns>
        [OperationContract]
        string SalesOrderSAP(Orders orders);
        /// <summary>
        /// GetData
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Response GetData(Request request);
        /// <summary>
        /// Genera una consulta desde Sap y actuliza el inventario existente en Magento.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        string LoadInventoryStockItems();
        /// <summary>
        /// Obtiene el consecutivo de los clientes desde Sap
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        string GetClientConsecutive();
        /// <summary>
        /// Reindexa  los indices en magento mediante commando ssh
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        string Reindex();
        /// <summary>
        /// Devuelve una orden de de venta
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        string GetSalesOrder(string sOrder);
        /// <summary>
        /// Realiza una transferencia de stock entre almacenes
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="StockTran"></param>
        /// <returns></returns>
        [OperationContract]
        int StockTranfer(List<StockTransfer> StockTran);
    }
 
    [MessageContract(IsWrapped = false)]
    public class Response
    {
        [MessageBodyMember]
        public GetDataResponse TestReply { get; set; }
    }

    [MessageContract(WrapperName = "GetData")]
    public class Request
    {
        [MessageBodyMember]
        public string name { get; set; }
    }

    [DataContract(Namespace = "http://www.celed.mx/types")]
    public class GetDataResponse
    {
        [DataMember(Name = "Result")]
        public Result Result { get; set; }

        [DataMember(Name = "ResultData")]
        public ResultData ResultData { get; set; }
    }

    [DataContract(Namespace = "http://www.celed.mx/types")]
    public class Result
    {
        [DataMember(Name = "ActionSuccessful")]
        public bool ActionSuccessful { get; set; }
    }

    [DataContract(Namespace = "http://www.celed.mx/types")]
    public class ResultData
    {
        [DataMember(Name = "Name")]
        public string Name { get; set; }
    }

    // Utilice un contrato de datos, como se ilustra en el ejemplo siguiente, para agregar tipos compuestos a las operaciones de servicio.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}

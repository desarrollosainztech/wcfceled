﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Reflection;
using System.Configuration;

namespace WcfService1
{
    public static class LogWriter
    {
     
        private static string m_exePath = string.Empty;
        private static string pathTmp = string.Empty;
        public static void LogWrite(string logMessage)
        {
            //m_exePath = Path.GetDirectoryName(ConfigurationManager.AppSettings["PathFile"].ToString());
            //pathTmp = m_exePath + "\\" + DateTime.Now.ToString("yyyy-MM-dd") + "_log.txt";

            //if (!File.Exists(pathTmp))
            //    File.Create(pathTmp).Close();

            //try
            //{
            //    using (StreamWriter w = File.AppendText(pathTmp))
            //    {
            //        AppendLog(logMessage, w);
            //        w.Close();
            //    }

            //}
            //catch (Exception ex)
            //{
            //    using (StreamWriter w = File.AppendText(pathTmp))
            //    {
            //        AppendLog(ex.Message, w);
            //        w.Close();
            //    }
            //}
            WriteLog(logMessage);

        }
        public static void WriteLog(string strLog)
        {
            StreamWriter log;
            FileStream fileStream = null;
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;
            try
            {
                m_exePath = Path.GetDirectoryName(ConfigurationManager.AppSettings["PathFile"].ToString());
                pathTmp = m_exePath + "\\" + DateTime.Now.ToString("yyyy-MM-dd") + "_log.txt";

                logFileInfo = new FileInfo(pathTmp);
                logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
                if (!logDirInfo.Exists) logDirInfo.Create();
                if (!logFileInfo.Exists)
                {
                    fileStream = logFileInfo.Create();
                }
                else
                {
                    fileStream = new FileStream(pathTmp, FileMode.Append);
                }
                log = new StreamWriter(fileStream);
                log.Write("\r\nLog Entry : ");
                log.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                log.WriteLine("  :");
                log.WriteLine("  :{0}", strLog);
                log.WriteLine("-------------------------------");
                log.Close();
            }
            catch
            {

            }
        }
        private static void AppendLog(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("\r\nLog Entry : ");
                txtWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                txtWriter.WriteLine("  :");
                txtWriter.WriteLine("  :{0}", logMessage);
                txtWriter.WriteLine("-------------------------------");
                txtWriter.Close();
                
            }
            catch
            {
            }
        }
    }
}
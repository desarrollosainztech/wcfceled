﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using Renci.SshNet;
using CeledDA;
using System.Threading;
using System.ComponentModel;
using ReallySimpleLog;

namespace WcfService1
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IServiceCeled
    {
        /// <summary>
        /// Prepara la creacion de un archivo con clientes nuevos a partir de una consulta de SAP
        /// TypeQuery  1 == Genera csv de clientes 
        /// TypeQuery  2 == Genera csv de Precios
        /// </summary>
        /// <param name="TypeQuery"></param>
        /// <returns></returns>
        public string GenerateFileQuerys(int TypeQuery)
        {

            string connetionString = string.Empty;
            SqlConnection connection;
            SqlCommand command;
            string sql = string.Empty;
            string action = string.Empty;
            string sNameFile = string.Empty;

            connetionString = ConfigurationManager.AppSettings["SQLConnectionDB"].ToString();
            //sql = ConfigurationManager.AppSettings["QueryClient"].ToString();
            if (TypeQuery == 1)
            {
                sql = "SELECT   e_mail                               AS email, " +
                              " 'base'                               AS _website, " +
                              " 'default'                            AS _store, " +
                              " '1'                                  AS website_id, " +
                              " '1'                                  AS store_id, " +
                              " createdate                           AS created_in, " +
                              " NULL                                 AS prefix, " +
                              " CASE " +
                               "  WHEN Charindex(' ', cardname) = 0 THEN cardname " +
                               "  WHEN Charindex(' ', cardname) = Patindex('% _[., ]%', cardname) THEN " +
                               "  Rtrim(" +
                               "  Substring(cardname, 1, Charindex(' ', cardname) + 2)) " +
                               "  ELSE Substring(cardname, 1, Charindex(' ', cardname))  " +
                             "  END[firstname],  " +
                              " CASE " +
                                " WHEN Charindex(' ', cardname) = 0 THEN '' " +
                                " WHEN Charindex(' ', cardname) = Patindex('% _[., ]%', cardname) THEN " +
                                " Ltrim(" +
                                " Substring(cardname, Charindex(' ', cardname) + 3, 1000)) " +
                                " ELSE Substring(cardname, Charindex(' ', cardname) +1, 1000)  " +
                              " END[lastname],  " +
                              " NULL AS suffix,  " +
                              " '1'                                  AS group_id," +
                              " NULL                                 AS dob," +
                              " NULL                                 AS password_hash," +
                              " NULL                                 AS taxvat," +
                              " NULL                                 AS confirmation," +
                              " createdate                           AS created_at," +
                              " NULL                                 AS gende," +
                              " NULL                                 AS g2k_cliente," +
                              " NULL                                 AS g2k_tercero," +
                              " NULL                                 AS g2k_tarifa," +
                              " NULL                                 AS g2k_agrupacion," +
                              " NULL                                 AS password," +
                              " city                                 AS _address_city," +
                              " address                              AS _address_company," +
                              " country                              AS _address_country_id," +
                              " fax                                  AS _address_fax," +
                              " CASE" +
                                " WHEN Charindex(' ', address) = 0 THEN address" +
                                " WHEN Charindex(' ', address) = Patindex('% _[., ]%', address) THEN" +
                                " Rtrim(" +
                                " Substring(address, 1, Charindex(' ', address) + 2))" +
                                " ELSE Substring(address, 1, Charindex(' ', address)) " +
                              " END[_address_firstname], " +
                              " CASE" +
                                " WHEN Charindex(' ', address) = 0 THEN ''" +
                                " WHEN Charindex(' ', address) = Patindex('% _[., ]%', address) THEN" +
                                " Ltrim(" +
                                " Substring(address, Charindex(' ', address) + 3, 1000))" +
                                " ELSE Substring(address, Charindex(' ', address) +1, 1000) " +
                              " END[_address_lastname], " +
                              " NULL AS _address_middlename, " +
                              " zipcode AS _address_postcode, " +
                              " NULL AS _address_prefix, " +
                              " (SELECT crd1.state" +
                               " FROM   crd1" +
                               " WHERE  crd1.cardcode = ocrd.cardcode " +
                                     "  AND crd1.address = 'ENTREGA') AS _address_region, " +
                              " (SELECT crd1.street" +
                              "  FROM   crd1" +
                              "  WHERE  crd1.cardcode = ocrd.cardcode" +
                                     "  AND crd1.address = 'ENTREGA') AS _address_street," +
                              " NULL                                 AS _address_suffix," +
                              " phone1                               AS _address_telephone," +
                              " (SELECT TOP 1 crd1.street + ' ' + crd1.block + ' ' + crd1.zipcode + ' ' + crd1.city + ' '" +
                                     "  + crd1.country + ' ' + crd1.state" +
                               " FROM   crd1" +
                               " WHERE  crd1.cardcode = ocrd.cardcode" +
                                     "  AND crd1.address = 'FACTURACION' ) AS _address_default_billing_," +
                              " (SELECT street + ' ' + block + ' ' + zipcode + ' ' + city + ' '" +
                                     "  + country + ' ' + state" +
                              "  FROM   crd1" +
                              "  WHERE  crd1.cardcode = ocrd.cardcode" +
                                     "  AND address = 'ENTREGA')     AS _address_default_shipping_," +
                              " lictradnum                           AS rfc" +
                       " FROM ocrd" +
                       " WHERE cardtype = 'C'" +
                              " AND e_mail IS NOT NULL" +
                              " AND frozenfor = 'N' ORDER BY CREATED_IN DESC; ";
                sNameFile = "CsvClient";
            }
            else
            {
                sql = "SELECT T0.ItemCode," +
                          " (T1.Price * (select rate from ORTT where ratedate  in (SELECT CONVERT(date, GETDATE() - 1)))) +" +
                           " ((T1.Price * (select rate from ORTT where ratedate  in (SELECT CONVERT(date, GETDATE() - 1)))) *4) / 100 as price " +
                      " FROM OITM T0 INNER JOIN ITM1 T1 ON T0.ItemCode = T1.ItemCode INNER " +
                   " JOIN OPLN T2 ON T1.PriceList = T2.ListNum " +
                   " where T2.ListNum = 5";

                sNameFile = "CsvPrice";
            }

            connection = new SqlConnection(connetionString);
            try
            {
                connection.Open();
                command = new SqlCommand(sql, connection);
                command.ExecuteNonQuery();

                DataTable dtClientes = new DataTable();
                using (SqlDataAdapter a = new SqlDataAdapter(command))
                {
                    a.Fill(dtClientes);
                    command.Dispose();
                    connection.Close();

                    action = GenerateCsvClient(dtClientes, sNameFile);
                    if (action.Equals(ConfigurationManager.AppSettings["Action"].ToString()))
                        UploadFileMagento(sNameFile);

                }
                ReallySimpleLog.ReallySimpleLog.WriteLog(sNameFile + "-> Successfull ");
                return "Successfull";
            }
            catch (Exception ex)
            {
                ReallySimpleLog.ReallySimpleLog.WriteLog(sNameFile + " -> Error :" + ex.Message);
                return "UnSuccessful";
            }

        }
        /// <summary>
        /// Genera un archivo  con clientes nuevos a partir de una consulta de SAP 
        /// </summary>
        /// <param name="dtClient"></param>
        /// <returns></returns>
        string GenerateCsvClient(DataTable dtClient, string sNameFile)
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                string[] columnNames = dtClient.Columns.Cast<DataColumn>().
                                                  Select(column => column.ColumnName).
                                                  ToArray();
                sb.AppendLine(string.Join(",", columnNames));

                foreach (DataRow row in dtClient.Rows)
                {
                    string[] fields = row.ItemArray.Select(field => field.ToString()).
                                                    ToArray();
                    sb.AppendLine(string.Join(",", fields));
                }
                string filename = ConfigurationManager.AppSettings["PathFile"].ToString() + sNameFile + ConfigurationManager.AppSettings["filename"].ToString();
                File.WriteAllText(filename, sb.ToString());

                ReallySimpleLog.ReallySimpleLog.WriteLog(sNameFile + " file created successfully " + DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
                return "Successfull";
            }
            catch (Exception ex)
            {
                ReallySimpleLog.ReallySimpleLog.WriteLog(sNameFile + "-> Error :" + ex.Message);
                return ex.Message;
            }

        }
        /// <summary>
        /// GetDataUsingDataContract
        /// </summary>
        /// <param name="composite"></param>
        /// <returns></returns>
        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
        /// <summary>
        /// GetData
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Response GetData(Request request)
        {
            return new Response
            {
                TestReply = new GetDataResponse
                {
                    Result = new Result { ActionSuccessful = true },
                    ResultData = new ResultData { Name = request.name }
                }
            };
        }
        /// <summary>
        /// Carga un archivo al servidor de www.saintech.com con cleintes nuevos a partir de una consulta de SAP 
        /// </summary>
        /// <returns></returns>
        private string UploadFileMagento(string sNameFile)
        {

            string server = ConfigurationManager.AppSettings["MageServer"].ToString();
            string User = ConfigurationManager.AppSettings["MageUser"].ToString();
            string Pass = ConfigurationManager.AppSettings["MagePass"].ToString();
            int Port = Convert.ToInt16(ConfigurationManager.AppSettings["MagePort"]);

            var client = new SftpClient(server, Port, User, Pass);
            client.Connect();

            try
            {
                FileInfo f = new FileInfo(ConfigurationManager.AppSettings["PathFile"].ToString() + sNameFile + ConfigurationManager.AppSettings["filename"].ToString());
                string uploadfile = f.FullName;

                if (client.IsConnected)
                {
                    var fileStream = new FileStream(uploadfile, FileMode.Open);
                    if (fileStream != null)
                    {
                        client.UploadFile(fileStream, "/home/mdaz/FilesSap/" + f.Name, null);
                        client.Disconnect();
                        client.Dispose();
                    }
                }
                ReallySimpleLog.ReallySimpleLog.WriteLog("File uploaded to magento successfully ");
                return "Successfull";
            }
            catch (Exception ex)
            {
                if (client.IsConnected)
                {
                    client.Disconnect();
                    client.Dispose();
                }
                ReallySimpleLog.ReallySimpleLog.WriteLog("UploadFileMagento -> Error :" + ex.Message);
                return ex.Message;
            }
        }
        /// <summary>
        /// Recibe una orden de venta proveniente de www.saintech.com
        /// </summary>
        /// <param name="orders"></param>
        /// <returns></returns>
        public string SalesOrderSAP(Orders orders)
        {
            SAPbobsCOM.Company oCompany;
            string sResponse = string.Empty;
            Connection SetCon = new Connection();
            oCompany = SetCon.GetConnection();
            if (oCompany.Connected == true) sResponse = CreateSalesOrder(oCompany, orders);
            else sResponse = "Failded, Please check to log";

            return sResponse;
        }
        /// <summary>
        /// Crea una orden de venta enviada de www.saintech.com a Sap
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="orders"></param>
        /// <returns></returns>
        private string CreateSalesOrder(SAPbobsCOM.Company oCompany, Orders orders)
        {

            SAPbobsCOM.Documents oOrder; // Order object 
            SAPbobsCOM.Recordset rs;
            SAPbobsCOM.Currencies oCurrencies;
            // Error handling variables 
            string sErrMsg;
            string dfcurrency;
            double dRate;
            int lErrCode;
            int lRetCode;
            int docNumber = 0;

            try
            {

                ReallySimpleLog.ReallySimpleLog.WriteLog("Starts data processing for submission to SAP ");
                oOrder = ((SAPbobsCOM.Documents)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders)));
                oCurrencies = ((SAPbobsCOM.Currencies)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oCurrencyCodes)));

                rs = null;
                // Create the next Order number 
                object sSQL = "SELECT TOP 1 DocNum FROM dbo.ORDR  where Series = " + orders.Series + " ORDER BY DocNum DESC";
                rs = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
                rs.DoQuery(System.Convert.ToString(sSQL));
                while (!((rs.EoF)))
                {
                    docNumber = System.Convert.ToInt32(rs.Fields.Item(0).Value) + 1;
                    rs.MoveNext();
                }
                ReallySimpleLog.ReallySimpleLog.WriteLog("Get the next Order number for submission to SAP " + docNumber + " ");
                // Default Currency 
                dfcurrency = ""; rs = null;
                sSQL = "SELECT SysCurrncy FROM dbo.OADM";
                rs = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
                rs.DoQuery(System.Convert.ToString(sSQL));
                while (!((rs.EoF)))
                {
                    dfcurrency = System.Convert.ToString(rs.Fields.Item(0).Value);
                    rs.MoveNext();
                }
                oCurrencies.GetByKey(dfcurrency);
                //oPriceLists.GetByKey("5");
                // Default rate tipo de cambio 
                rs = null;
                dRate = 0;
                sSQL = "select Rate from ORTT  where RateDate in(select max(RateDate) - 1 as RateDate from ORTT)";
                rs = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
                rs.DoQuery(System.Convert.ToString(sSQL));
                while (!((rs.EoF)))
                {
                    dRate = System.Convert.ToDouble(rs.Fields.Item(0).Value);
                    rs.MoveNext();
                }

                ReallySimpleLog.ReallySimpleLog.WriteLog("Set properties of the Order Header : " + docNumber + " object for submission to SAP ");
                // Set properties of the Order object 
                oOrder.CardCode = orders.CardCode;
                oOrder.HandWritten = SAPbobsCOM.BoYesNoEnum.tNO;
                oOrder.DocNum = docNumber;
                oOrder.DocDate = System.DateTime.Today;
                oOrder.DocDueDate = System.DateTime.Today.AddDays(30);
                oOrder.PaymentMethod = orders.PaymentMethod;
                oOrder.SalesPersonCode = orders.SalesPersonCode;
                oOrder.NumAtCard = orders.NumAtCard;
                oOrder.DiscountPercent = 0;
                oOrder.Series = orders.Series;
                oOrder.Comments = orders.Comment;//ConfigurationManager.AppSettings.Get("Comment").ToString();
                ReallySimpleLog.ReallySimpleLog.WriteLog("Finished set properties of the Order Header : " + docNumber + " object for submission to SAP ");

                List<OrderItem> orderitem = new List<OrderItem>();
                orderitem = orders.items;
                ReallySimpleLog.ReallySimpleLog.WriteLog("Set properties of the Order Detail Number: " + docNumber + " for submission to SAP ");

                int i = 0;
                foreach (var item in orderitem)
                {
                    //double dlineTotal = 0;
                    //double dTotal = 0;
                    oOrder.Lines.SetCurrentLine(i);
                    oOrder.Lines.Currency = "MXN";
                    oOrder.Lines.ItemCode = item.ItemCode;
                    oOrder.Lines.ItemDescription = item.ItemDescription;
                    oOrder.Lines.Quantity = item.Quantity;
                    oOrder.Lines.UnitPrice = item.UnitPrice;
                    oOrder.Lines.TaxCode = item.TaxCode;
                    oOrder.Lines.WarehouseCode = item.WarehouseCode;
                    //Si se le envia el total por linea el campo de descuento ya no funciona.. pinches mamadas!!!
                    //dlineTotal = item.UnitPrice * item.Quantity;
                    //dTotal = dlineTotal;
                    //oOrder.Lines.LineTotal = dTotal;
                    oOrder.Lines.DiscountPercent = item.DiscountPercent;

                    ReallySimpleLog.ReallySimpleLog.WriteLog("Set properties of the Order Detail Item Values: ItemCode : " + item.ItemCode + System.Environment.NewLine +
                                                                                                             "ItemDescription : " + item.ItemDescription + System.Environment.NewLine +
                                                                                                             "Quantity : " + item.Quantity + System.Environment.NewLine +
                                                                                                             "Price : " + item.UnitPrice + System.Environment.NewLine +
                                                                                                             "TaxCode :  " + item.TaxCode + System.Environment.NewLine +
                                                                                                             "LineTotal : " + item.LineTotal + System.Environment.NewLine +
                                                                                                             "DiscountPercent : " + item.DiscountPercent + System.Environment.NewLine +
                                                                                                             "for submission to SAP ");
                    oOrder.Lines.Add();
                    i++;
                }


                lRetCode = oOrder.Add(); // add the order to the database 


                if (lRetCode != 0)
                {
                    oCompany.GetLastError(out lErrCode, out sErrMsg);
                    oCompany.Disconnect();
                    ReallySimpleLog.ReallySimpleLog.WriteLog("Set properties of the Order Detail Number: " + docNumber + " for submission to SAP ");
                    return sErrMsg;
                }
                else
                {
                    oCompany.Disconnect();
                    ReallySimpleLog.ReallySimpleLog.WriteLog("Successful ShipmentOrder Order: " + docNumber + " ");
                }
            }
            catch (Exception ex)
            {
                ReallySimpleLog.ReallySimpleLog.WriteLog("Error -> Order: " + docNumber + " " + ex.Message + " " + ex.InnerException + " ");
                return ex.Message;
            }

            return docNumber.ToString(); ;
        }
        /// <summary>
        /// Genera una consulta desde Sap y actuliza el inventario existente en Magento.
        /// </summary>
        /// <returns></returns>
        public string LoadInventoryStockItems()
        {
            SAPbobsCOM.Recordset rs;
            SAPbobsCOM.Company oCompany;
            Saniztech.MagentoService _magentoService = new Saniztech.MagentoService();

            string url = ConfigurationManager.AppSettings["apiUrl"].ToString();
            _magentoService.Url = url;
            string apiUser = ConfigurationManager.AppSettings["apiUser"].ToString();
            string apiKey = ConfigurationManager.AppSettings["apiKey"].ToString();
            var sessionId = _magentoService.login(apiUser, apiKey);
            ReallySimpleLog.ReallySimpleLog.WriteLog("Starts LoadInventoryStockItems to SAP " + sessionId.ToString());
            Saniztech.catalogInventoryStockItemUpdateEntity uStock = new Saniztech.catalogInventoryStockItemUpdateEntity();
            Connection SetCon = new Connection();
            oCompany = SetCon.GetConnection();

            try
            {

                if (oCompany.Connected == true)
                {
                    object sSQL = "SELECT T1.whsname, " +
                                          " T0.itemcode, " +
                                          " Sum(T0.inqty - T0.outqty) AS 'On Hand'" +
                                   " FROM dbo.oinm T0" +
                                         "  INNER JOIN dbo.owhs T1" +
                                                 "  ON T0.warehouse = T1.whscode" +
                                   " WHERE T1.whsname = 'GENERAL' " +
                                   " GROUP BY T1.whsname, " +
                                             " T0.itemcode" +
                                   " HAVING Sum(T0.inqty - T0.outqty) > 0 ";

                    rs = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
                    rs.DoQuery(System.Convert.ToString(sSQL));
                    while (!((rs.EoF)))
                    {

                        uStock.qty = Convert.ToString(rs.Fields.Item(2).Value);
                        uStock.is_in_stock = 1;
                        uStock.manage_stock = 1;
                        uStock.use_config_manage_stock = 0;
                        int result;
                        try
                        {
                            result = _magentoService.catalogInventoryStockItemUpdate(sessionId, Convert.ToString(rs.Fields.Item(1).Value), uStock);
                            ReallySimpleLog.ReallySimpleLog.WriteLog("Items to SAP : " + Convert.ToString(rs.Fields.Item(1).Value) + " Quantity : " + Convert.ToString(rs.Fields.Item(2).Value) + " ");
                        }
                        catch (Exception ex)
                        {
                            ReallySimpleLog.ReallySimpleLog.WriteLog("Error ->Items to SAP: " + Convert.ToString(rs.Fields.Item(1).Value) + " Quantity: " + Convert.ToString(rs.Fields.Item(2).Value) + ex.Message + "  " + "  " + ex.InnerException + " ");
                        }

                        ReallySimpleLog.ReallySimpleLog.WriteLog(" catalogInventoryStockItemUpdate - > successfully ");
                        rs.MoveNext();
                    }

                    oCompany.Disconnect();
                    _magentoService.endSession(sessionId);
                    Reindex();
                }
                else
                {
                    ReallySimpleLog.ReallySimpleLog.WriteLog("Warning! Connection Sap - > False ");
                }
            }
            catch (Exception ex)
            {
                ReallySimpleLog.ReallySimpleLog.WriteLog("Error ->Items to SAP: " + ex.Message + " " + ex.InnerException + " ");
                oCompany.Disconnect();
                _magentoService.endSession(sessionId);
                return "Failed";
            }
            return "successfully";
        }
        /// <summary>
        ///  Obtiene el consecutivo de los clientes desde Sap
        /// </summary>
        /// <returns></returns>
        public string GetClientConsecutive()
        {
            SAPbobsCOM.Recordset rs;
            SAPbobsCOM.Company oCompany;

            string sConsecutive = string.Empty;
            ReallySimpleLog.ReallySimpleLog.WriteLog("Starts GetClientConsecutive to SAP ");
            Connection SetCon = new Connection();
            oCompany = SetCon.GetConnection();

            try
            {

                if (oCompany.Connected == true)
                {
                    object sSQL = "select  'C'+Right('0000000' + CONVERT(NVARCHAR,max(substring( cardcode ,2 , 7 )) + 1),7) from [SBOOPERADORA].[DBO].ocrd where cardcode like 'C%'";
                    rs = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
                    rs.DoQuery(System.Convert.ToString(sSQL));
                    while (!((rs.EoF)))
                    {
                        sConsecutive = Convert.ToString(rs.Fields.Item(0).Value);
                        rs.MoveNext();
                    }

                    oCompany.Disconnect();

                }
                else
                {
                    ReallySimpleLog.ReallySimpleLog.WriteLog("Warning! Connection Sap - > False ");
                    sConsecutive = "0";
                }
            }
            catch (Exception ex)
            {
                ReallySimpleLog.ReallySimpleLog.WriteLog("Error -> : " + ex.Message + " " + ex.InnerException + " ");
                oCompany.Disconnect();
                return sConsecutive = "0";
            }
            return sConsecutive;
        }
        /// <summary>
        ///  Obtiene una orden de venta
        /// </summary>
        /// <returns></returns>
        public string GetSalesOrder(string sOrder)
        {
            //-------------------------
            //Declaring SAP objects...
            //-------------------------
            SAPbobsCOM.Company oCompany;
            string srOrder = string.Empty;
            ReallySimpleLog.ReallySimpleLog.WriteLog("Starts GetClientConsecutive to SAP ");
            Connection SetCon = new Connection();
            oCompany = SetCon.GetConnection();

            try
            {

                if (oCompany.Connected == true)
                {

                    bool RetVal;
                    int ErrCode;
                    string ErrMsg;
                    SAPbobsCOM.Documents oOrder;
                    oOrder = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders);

                    // 'Retrieve the document record to close from the database
                    oOrder.Series = 9;
                    RetVal = oOrder.GetByKey(Convert.ToInt32(sOrder));

                    if (RetVal == false)
                    {
                        oCompany.GetLastError(out ErrCode, out ErrMsg);
                        oCompany.Disconnect();
                        return srOrder = "0";
                    }
                    else
                    {
                        srOrder = oOrder.DocEntry.ToString();
                        oOrder.Close();
                        oCompany.Disconnect();
                    }
                }
                else
                {
                    ReallySimpleLog.ReallySimpleLog.WriteLog("Warning! Connection Sap - > False ");
                    return srOrder = "0";
                }
                return srOrder;
            }
            catch (Exception ex)
            {
                ReallySimpleLog.ReallySimpleLog.WriteLog("Error -> : " + ex.Message + " " + ex.InnerException + " ");
                oCompany.Disconnect();
                return "0";
            }

        }
        /// <summary>
        /// Reindexa los indices de magento via commando ssh
        /// </summary>
        /// <returns></returns>
        public string Reindex()
        {

            string server = ConfigurationManager.AppSettings["MageServer"].ToString();
            string User = ConfigurationManager.AppSettings["MageUser"].ToString();
            string Pass = ConfigurationManager.AppSettings["MagePass"].ToString();
            int Port = Convert.ToInt16(ConfigurationManager.AppSettings["MagePort"]);
            string sComando = ConfigurationManager.AppSettings["reindex"].ToString();
            var client = new SshClient(server, Port, User, Pass);

            client.Connect();

            SshCommand cmd = client.RunCommand(string.Format(sComando));

            string sResultado = cmd.Result;
            int iResultado = cmd.ExitStatus;
            client.Disconnect();
            client.Dispose();

            if (!iResultado.ToString().Trim().Equals("0"))
            {
                ReallySimpleLog.ReallySimpleLog.WriteLog("Error Reindex: " + sResultado + " ");
            }
            else
            {
                ReallySimpleLog.ReallySimpleLog.WriteLog("Reindex: OK " + sResultado + " ");
            }

            return "successfully";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="StockTran"></param>
        /// <returns></returns>
        public int StockTranfer(List<StockTransfer> StockTran)
        {
            int return_value;
            int lErrCode;
            string sErrMsg;
            if(StockTran.ToList().Count()<1)
            {
                ReallySimpleLog.ReallySimpleLog.WriteLog("No data found " + StockTran.ToList().ToString());
                return   return_value = 0;
            }
            SAPbobsCOM.Company oCompany;
            string sResponse = string.Empty;
            Connection SetCon = new Connection();
            oCompany = SetCon.GetConnection();
            ReallySimpleLog.ReallySimpleLog.WriteLog("Connected SAP! ");
            
            SAPbobsCOM.StockTransfer oStockDoc;
            oStockDoc = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oStockTransfer);
            foreach (var item in StockTran)
            {

                oStockDoc.FromWarehouse = item.FromWarehouse;
                oStockDoc.Lines.ItemCode = item.ItemCode;
                oStockDoc.Lines.Quantity = item.Quantity;
                oStockDoc.Lines.WarehouseCode = item.WarehouseCode;
                oStockDoc.Lines.Add();
                ReallySimpleLog.ReallySimpleLog.WriteLog("line Add Succes!! "+ item.FromWarehouse +" - " + item.ItemCode + " - " + item.Quantity + " - " + item.WarehouseCode);

            }
            return_value = oStockDoc.Add();


            if (return_value != 0)
            {
                oCompany.GetLastError(out lErrCode, out sErrMsg);
                oCompany.Disconnect();
                ReallySimpleLog.ReallySimpleLog.WriteLog("Set properties of the StockTransfer: " + return_value + " for submission to SAP " +
                    lErrCode +" " + sErrMsg);
                return_value = 0;
            }
            else
            {
                oCompany.Disconnect();
                ReallySimpleLog.ReallySimpleLog.WriteLog("Successful StockTransfer: " + return_value + " ");
            }

            return return_value;
        }
    }
    }

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Reflection;
using System.Configuration;

namespace WcfService1
{
    public static class LogWriter
    {
     
        private static string m_exePath = string.Empty;
        private static string pathTmp = string.Empty;
        public static void LogWrite(string logMessage)
        {   
            m_exePath = Path.GetDirectoryName(ConfigurationManager.AppSettings["PathFile"].ToString());
            pathTmp = m_exePath + "\\" + DateTime.Now.ToString("yyyy-MM-dd") + "_log.txt";

            if (!File.Exists(pathTmp))
                File.Create(pathTmp);

            try
            {
                using (FileStream fs = new FileStream(pathTmp, FileMode.Append))
                {
                    fs.Close();
                }
                using (StreamWriter w = File.AppendText(pathTmp))
                    AppendLog(logMessage, w);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        private static void AppendLog(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("\r\nLog Entry : ");
                txtWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                txtWriter.WriteLine("  :");
                txtWriter.WriteLine("  :{0}", logMessage);
                txtWriter.WriteLine("-------------------------------");
            }
            catch
            {
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CeledDA
{
    public class OrderItem
    {
        // set properties of the Order object detail
        /// <summary>
        /// /Clave del Producto
        /// </summary>
        public string ItemCode  { get; set; }
        /// <summary>
        /// / Descripcion del Producto
        /// </summary>
        public string ItemDescription  { get; set; }
        /// <summary>
        /// /Cantidad
        /// </summary>
        public double Quantity  { get; set; }
        /// <summary>
        /// /Precio Unitario
        /// </summary>
        public double UnitPrice { get; set; }
        /// <summary>
        /// /Valor de Impuesto
        /// </summary>
        public string TaxCode  { get; set; }
        /// <summary>
        /// /Total por linea
        /// </summary>
        public double LineTotal  { get; set; }
        /// <summary>
        /// Descuentoi
        /// </summary>
        public double DiscountPercent { get; set; }
        /// <summary>
        /// Almacen
        /// </summary>
        public string WarehouseCode { get; set; }


    }

    public class Orders
    {
        // set properties of the Order object header
        /// <summary>
        /// /Clave del Cliente
        /// </summary>
        public string CardCode { get; set; }
        /// <summary>
        /// /Numero de Documento SAP
        /// </summary>
        private int DocNum { get; set; }
        /// <summary>
        /// /Fecha Creacion
        /// </summary>
        public DateTime DocDate { get; set; }
        /// <summary>
        /// /Fecha Actulizacion
        /// </summary>
        public DateTime DocDueDate { get; set; }
        /// <summary>
        /// /Tipo de Moneda
        /// </summary>
        private string DocCurrency { get; set; }
        /// <summary>
        /// /Forma de Pago
        /// </summary>
        public string NumAtCard { get; set; }
        /// <summary>
        /// /Serie del Docuemnto
        /// </summary>
        public int Series { get; set; }
        /// <summary>
        /// /Forma de pago
        /// </summary>
        public string PaymentMethod { get; set;}
        /// <summary>
        /// /Comentario
        /// </summary>
        public string Comment { get; set; }
        /// <summary>
        /// Clave de vendedor
        /// </summary>
        public int SalesPersonCode { get; set; }

        /// <summary>
        /// /Lista de items
        /// </summary>
        public List<OrderItem> items { get; set; }
  }

}

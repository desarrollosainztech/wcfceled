﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CeledDA
{
    public class StockTransfer
    {
        /// <summary>
        /// /Clave del Producto
        /// </summary>
        public string CardCode { get; set; }
        /// <summary>
        /// codigo
        /// </summary>
        public DateTime DocDate { get; set; }
        /// <summary>
        /// fecha de movimiento
        /// </summary>
        public string ItemCode { get; set; }
        /// <summary>
        /// Cantidad
        /// </summary>
        public double Quantity { get; set; }
        /// <summary>
        /// Almacen destino
        /// </summary>

        public string WarehouseCode { get; set; }
        /// <summary>
        /// Almacen origen
        /// </summary>
        public string FromWarehouse { get; set; }
    }
}

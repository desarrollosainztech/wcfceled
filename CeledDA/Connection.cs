﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace CeledDA
{
    public class Connection
    {
        private SAPbobsCOM.Company oCompany;
        // Error handling variables 
        public static string sErrMsg;
        public static int lErrCode;
        public static int lRetCode;
        private void SetApplication()
        {
            try
            {
               
                #region SAP Conection
                int lRetCode, ErrorCode;
                String ErrorMessage;
                
                oCompany = new SAPbobsCOM.Company();  
                oCompany.Server = ConfigurationManager.AppSettings["Server"];
                oCompany.CompanyDB = ConfigurationManager.AppSettings.Get("CompanyDB").ToString();
                oCompany.UserName = ConfigurationManager.AppSettings.Get("UserName").ToString();
                oCompany.Password = ConfigurationManager.AppSettings.Get("Password").ToString();
                oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                oCompany.DbUserName = ConfigurationManager.AppSettings.Get("DbUserName").ToString();
                oCompany.DbPassword = ConfigurationManager.AppSettings.Get("DbPassword").ToString();
                oCompany.LicenseServer = ConfigurationManager.AppSettings.Get("LicenseServer").ToString();
                oCompany.language = SAPbobsCOM.BoSuppLangs.ln_Spanish; 
                oCompany.UseTrusted = false;

                lRetCode = oCompany.Connect();
                #endregion
                if (lRetCode != 0)
                {
                    oCompany.GetLastError(out ErrorCode, out ErrorMessage);
                }
                else ReallySimpleLog.ReallySimpleLog.WriteLog("Connection Succes.... State Connected ");
             
            }
            catch(Exception ex)
            {
                ReallySimpleLog.ReallySimpleLog.WriteLog("Connection Sap Failed....State Disconnect  -> Error :" + ex.Message );
                if (oCompany.Connected == true)
                {
                    oCompany.Disconnect();
                }
            }

        }

        public Connection()
        {
           
        }
        public SAPbobsCOM.Company GetConnection()
        {
            SetApplication();
            return oCompany;
        }
    }
}
